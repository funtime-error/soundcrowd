import React from 'react';

function Loading() {
  return (
    <div className="loading">
      <progress className="progress is-small is-primary" max="100"></progress>
    </div>
  );
}

export default Loading;
