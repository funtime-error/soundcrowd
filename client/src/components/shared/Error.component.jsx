import React, { useState } from 'react';

function Error({ error }) {
  const [display, setDisplay] = useState(true);

  return (
    display && (
      <div className="notification is-danger">
        <button className="delete" onClick={() => setDisplay(false)}></button>
        {error.message}
      </div>
    )
  );
}

export default Error;
