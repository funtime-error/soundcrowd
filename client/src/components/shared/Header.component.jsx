import React from 'react';
import { ApolloConsumer } from '@apollo/react-hooks';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';

function Header({ currentUser }) {
  const handleSignout = client => {
    localStorage.removeItem('authToken');
    client.writeData({ data: { isLoggedIn: false } });
    console.log('Signed out user', client);
  };

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <Link className="navbar-item is-uppercase has-text-primary" to="/">
          <strong>SoundCrowd</strong>
        </Link>
      </div>

      <div className="navbar-menu">
        <div className="navbar-end">
          <div className="navbar-item has-dropdown is-hoverable">
            <Link className="navbar-link" to={`/profile/${currentUser.id}`}>
              <span className="icon">
                <FontAwesomeIcon icon={faUserCircle} />
              </span>
              <span>{currentUser.username}</span>
            </Link>
            {currentUser ? (
              <div className="navbar-dropdown is-right">
                <Link className="navbar-item" to={`/profile/${currentUser.id}`}>
                  Profile
                </Link>
                <ApolloConsumer>
                  {client => (
                    <Link
                      className="navbar-item"
                      to="/"
                      onClick={() => handleSignout(client)}>
                      Sign out
                    </Link>
                  )}
                </ApolloConsumer>
                <hr className="navbar-divider" />
                <div className="navbar-item">Version 0.1.0</div>
              </div>
            ) : (
              <Link className="button is-primary" to="/">
                <strong>Sign up</strong>
              </Link>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;
