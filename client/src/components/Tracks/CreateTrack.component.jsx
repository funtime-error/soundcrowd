import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload } from '@fortawesome/free-solid-svg-icons';

import Error from 'components/shared/Error.component';
import { GET_TRACKS_QUERY } from 'pages/Tracks/Tracks.component';

function CreateTrack() {
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [file, setFile] = useState('');
  const [submitting, setSubmitting] = useState(false);
  const [fileError, setFileError] = useState('');

  const [createTrack, { error }] = useMutation(CREATE_TRACK_MUTATION, {
    onCompleted: data => {
      setSubmitting(false);
      setOpen(false);
      setTitle('');
      setDescription('');
      setFile('');
    },
    update: (cache, { data: { createTrack } }) => {
      const { tracks } = cache.readQuery({ query: GET_TRACKS_QUERY });
      cache.writeQuery({
        query: GET_TRACKS_QUERY,
        data: { tracks: tracks.concat(createTrack.track) }
      });
    }
  });

  const handleFileChange = event => {
    const selectedFile = event.target.files[0];
    const FILE_SIZE_LIMIT = 10000000; // 10MB

    if (selectedFile && selectedFile.size > FILE_SIZE_LIMIT) {
      setFileError(`${selectedFile.name}: File size exceeds limit of 10MB!`);
    } else {
      setFile(selectedFile);
      setFileError('');
    }
  };

  const handleAudioUpload = async () => {
    const cloudName = 'funtime';
    const resourceType = 'video';
    const uploadPreset = 'soundcrowd';

    try {
      const data = new FormData();
      data.append('file', file);
      data.append('resource_type', resourceType);
      data.append('upload_preset', uploadPreset);
      data.append('cloud_name', cloudName);

      const response = await axios.post(
        `https://api.cloudinary.com/v1_1/${cloudName}/${resourceType}/upload`,
        data
      );

      return response.data.secure_url;
    } catch (error) {
      setSubmitting(false);
    }
  };

  const handleSubmit = async (event, createTrack) => {
    event.preventDefault();
    setSubmitting(true);
    const uploadedUrl = await handleAudioUpload();
    createTrack({ variables: { title, description, url: uploadedUrl } });
  };

  return (
    <>
      <button
        className={`button ${open && 'is-loading'}`}
        onClick={() => setOpen(true)}>
        Upload
      </button>
      <div className={`modal ${open ? 'is-active' : ''}`}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Basic Info</p>
            <button
              className="delete"
              aria-label="close"
              onClick={() => setOpen(false)}></button>
          </header>
          <section className="modal-card-body">
            <form
              id="create-track-form"
              onSubmit={event => handleSubmit(event, createTrack)}>
              <div className="field">
                <label htmlFor="title" className="label">
                  Title
                </label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    id="title"
                    value={title}
                    placeholder="Name your track"
                    onChange={event => setTitle(event.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label htmlFor="description" className="label">
                  Description
                </label>
                <div className="control">
                  <textarea
                    className="textarea"
                    id="description"
                    value={description}
                    placeholder="Describe your track"
                    onChange={event => setDescription(event.target.value)}
                    rows="6"
                  />
                </div>
              </div>
              <div className={`field file has-name ${file && 'is-primary'}`}>
                <label className="file-label">
                  <input
                    className="file-input"
                    type="file"
                    accept="audio/mp3,audio/wav"
                    name="audio"
                    id="audio"
                    onChange={handleFileChange}
                    required
                  />
                  <span className="file-cta">
                    <FontAwesomeIcon icon={faUpload} />
                    <span className="file-label">Choose file to upload</span>
                  </span>
                  {file && <span className="file-name">{file.name}</span>}
                </label>
              </div>
              {fileError && (
                <div className="notification is-danger is-light">
                  <button
                    className="delete"
                    disabled={submitting}
                    type="button"
                    onClick={() => setFileError(false)}
                  />
                  {fileError}
                </div>
              )}
              {error && <Error error={error} />}
            </form>
          </section>
          <footer className="modal-card-foot buttons is-right">
            <button
              className="button"
              disabled={submitting}
              type="button"
              onClick={() => setOpen(false)}>
              Cancel
            </button>
            <button
              disabled={
                submitting || !title.trim() || !description.trim() || !file
              }
              className={`button is-primary ${submitting && 'is-loading'}`}
              form="create-track-form"
              type="submit">
              Save
            </button>
          </footer>
        </div>
      </div>
    </>
  );
}

const CREATE_TRACK_MUTATION = gql`
  mutation($title: String!, $description: String!, $url: String!) {
    createTrack(title: $title, description: $description, url: $url) {
      track {
        id
        title
        description
        url
        likes {
          id
        }
        postedBy {
          id
          username
        }
      }
    }
  }
`;

export default CreateTrack;
