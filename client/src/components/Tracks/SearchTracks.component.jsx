import React, { useState, useRef } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

function SearchTracks({ setSearchResults }) {
  const [search, setSearch] = useState('');
  const searchInput = useRef();

  const client = useApolloClient();

  const clearSearchInput = () => {
    setSearchResults([]);
    setSearch('');
    searchInput.current.focus();
  };

  const handleSubmit = async event => {
    event.preventDefault();
    const response = await client.query({
      query: SEARCH_TRACKS_QUERY,
      variables: { search }
    });
    setSearchResults(response.data.tracks);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="field has-addons has-addons-centered">
        <div className="control">
          <button className="button" type="button" onClick={clearSearchInput}>
            <span className="icon">
              <FontAwesomeIcon icon={faTimes} size="lg" />
            </span>
          </button>
        </div>
        <div className="control is-expanded">
          <input
            className="input"
            type="text"
            placeholder="Search for artists, bands, tracks, podcasts"
            onChange={event => setSearch(event.target.value)}
            value={search}
            ref={searchInput}
          />
        </div>
        <div className="control">
          <button className="button" type="submit">
            <span className="icon">
              <FontAwesomeIcon icon={faSearch} size="lg" />
            </span>
          </button>
        </div>
      </div>
    </form>
  );
}

const SEARCH_TRACKS_QUERY = gql`
  query($search: String) {
    tracks(search: $search) {
      id
      title
      description
      url
      likes {
        id
      }
      postedBy {
        id
        username
      }
    }
  }
`;

export default SearchTracks;
