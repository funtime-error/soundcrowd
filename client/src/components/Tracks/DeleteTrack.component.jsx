import React, { useContext } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { UserContext } from 'App.component';
import { GET_TRACKS_QUERY } from 'pages/Tracks/Tracks.component';

function DeleteTrack({ track }) {
  const currentUser = useContext(UserContext);
  const isUploadedByCurrentUser = currentUser.id === track.postedBy.id;

  const [deleteTrack] = useMutation(DELETE_TRACK_MUTATION, {
    variables: { trackId: track.id },
    onCompleted: data => {
      console.log({ data });
    },
    update: (cache, { data: { deleteTrack } }) => {
      const data = cache.readQuery({ query: GET_TRACKS_QUERY });
      const index = data.tracks.findIndex(
        track => Number(track.id) === deleteTrack.trackId
      );
      const tracks = [
        ...data.tracks.slice(0, index),
        ...data.tracks.slice(index + 1)
      ];
      cache.writeQuery({ query: GET_TRACKS_QUERY, data: { tracks } });
    }
  });

  return (
    isUploadedByCurrentUser && (
      <button className="button" onClick={deleteTrack}>
        <span className="icon">
          <FontAwesomeIcon icon={faTrash} />
        </span>
        <span>Delete track</span>
      </button>
    )
  );
}

const DELETE_TRACK_MUTATION = gql`
  mutation($trackId: Int!) {
    deleteTrack(trackId: $trackId) {
      trackId
    }
  }
`;

export default DeleteTrack;
