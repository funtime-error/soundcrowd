import React from 'react';
import { Link } from 'react-router-dom';

import AudioPlayer from 'components/shared/AudioPlayer.component';
import DeleteTrack from 'components/Tracks/DeleteTrack.component';
import LikeTrack from 'components/Tracks/LikeTrack.component';
import UpdateTrack from 'components/Tracks/UpdateTrack.component';

function TrackList({ tracks }) {
  return (
    <div>
      {tracks.map(track => (
        <div className="box" key={track.id}>
          <div className="title">{track.title}</div>
          <div className="subtitle">
            <Link to={`/profile/${track.postedBy.id}`}>
              {track.postedBy.username}
            </Link>
          </div>
          <AudioPlayer url={track.url} />
          <div className="buttons are-small">
            <LikeTrack trackId={track.id} likeCount={track.likes.length} />
            <UpdateTrack track={track} />
            <DeleteTrack track={track} />
          </div>
        </div>
      ))}
    </div>
  );
}

export default TrackList;
