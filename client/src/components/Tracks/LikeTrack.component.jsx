import React, { useContext } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

import { UserContext, ME_QUERY } from 'App.component';

function LikeTrack({ trackId, likeCount }) {
  const currentUser = useContext(UserContext);
  const isAlreadyLikedByCurrentUser =
    currentUser.likeSet.findIndex(like => like.track.id === trackId) > -1;

  const [createLike] = useMutation(CREATE_LIKE_MUTATION, {
    variables: { trackId },
    onCompleted: data => {
      console.log({ data });
    },
    refetchQueries: [{ query: ME_QUERY }]
  });

  return (
    <button
      className="button is-small"
      disabled={isAlreadyLikedByCurrentUser}
      type="button"
      onClick={event => {
        event.stopPropagation();
        createLike();
      }}>
      <span className="icon">
        <FontAwesomeIcon icon={faHeart} />
      </span>
      <span>{likeCount}</span>
    </button>
  );
}

const CREATE_LIKE_MUTATION = gql`
  mutation($trackId: Int!) {
    createLike(trackId: $trackId) {
      track {
        id
        likes {
          id
        }
      }
    }
  }
`;

export default LikeTrack;
