import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import Error from 'components/shared/Error.component';

const LOGIN_MUTATION = gql`
  mutation($username: String!, $password: String!) {
    tokenAuth(username: $username, password: $password) {
      token
    }
  }
`;

function Register({ setNewUser }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const [tokenAuth, { loading, error, client }] = useMutation(LOGIN_MUTATION, {
    variables: { username, password }
  });

  const handleSubmit = async (event, tokenAuth, client) => {
    event.preventDefault();
    const response = await tokenAuth();
    localStorage.setItem('authToken', response.data.tokenAuth.token);
    client.writeData({ data: { isLoggedIn: true } });
  };

  return (
    <div className="card">
      <div className="card-header">
        <div className="card-header-title is-centered">Log In</div>
      </div>
      <div className="card-content">
        <form onSubmit={event => handleSubmit(event, tokenAuth, client)}>
          <div className="field">
            <label htmlFor="username" className="label">
              Username
            </label>
            <div className="control">
              <input
                type="text"
                className="input"
                id="username"
                onChange={event => setUsername(event.target.value)}
              />
            </div>
          </div>
          <div className="field">
            <label htmlFor="password" className="label">
              Password
            </label>
            <div className="control">
              <input
                type="password"
                className="input"
                id="password"
                onChange={event => setPassword(event.target.value)}
              />
            </div>
          </div>
          <button
            className={`button is-block is-primary ${
              loading ? 'is-loading' : ''
            }`}
            type="submit"
            disabled={loading || !username.trim() || !password.trim()}>
            Log In
          </button>
          <a href="#" onClick={() => setNewUser(true)}>
            New user? Register here.
          </a>
        </form>
      </div>
      {error && <Error error={error} />}
    </div>
  );
}

export default Register;
