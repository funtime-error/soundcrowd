import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import Error from 'components/shared/Error.component';

const REGISTER_MUTATION = gql`
  mutation($username: String!, $email: String!, $password: String!) {
    createUser(username: $username, email: $email, password: $password) {
      user {
        username
        email
      }
    }
  }
`;

function Register({ setNewUser }) {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [open, setOpen] = useState(false);

  const [createUser, { loading, error }] = useMutation(REGISTER_MUTATION, {
    variables: { username, email, password },
    onCompleted: data => {
      console.log({ data });
      setOpen(true);
    }
  });

  const handleSubmit = (event, createUser) => {
    event.preventDefault();
    createUser();
  };

  return (
    <div className="card">
      <div className="card-header">
        <div className="card-header-title is-centered">Register</div>
      </div>
      <div className="card-content">
        <form onSubmit={event => handleSubmit(event, createUser)}>
          <div className="field">
            <label htmlFor="username" className="label">
              Username
            </label>
            <div className="control">
              <input
                type="text"
                className="input"
                id="username"
                onChange={event => setUsername(event.target.value)}
              />
            </div>
          </div>
          <div className="field">
            <label htmlFor="email" className="label">
              Email
            </label>
            <div className="control">
              <input
                type="email"
                className="input"
                id="email"
                onChange={event => setEmail(event.target.value)}
              />
            </div>
          </div>
          <div className="field">
            <label htmlFor="password" className="label">
              Password
            </label>
            <div className="control">
              <input
                type="password"
                className="input"
                id="password"
                onChange={event => setPassword(event.target.value)}
              />
            </div>
          </div>
          <button
            className={`button is-block is-primary ${
              loading ? 'is-loading' : ''
            }`}
            type="submit"
            disabled={
              loading || !username.trim() || !email.trim() || !password.trim()
            }>
            Register
          </button>
          <a href="#" className="" onClick={() => setNewUser(false)}>
            Already have an account? Log in here.
          </a>
        </form>
      </div>
      <div className={`modal ${open ? 'is-active' : ''}`}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">User successfully created!</p>
            <button
              className="delete"
              aria-label="close"
              onClick={() => setOpen(false)}></button>
          </header>
          <section className="modal-card-body">
            Please log in to continue.
          </section>
          <footer className="modal-card-foot">
            <button
              className="button is-success"
              aria-label="close"
              onClick={() => setNewUser(false)}>
              Log In
            </button>
          </footer>
        </div>
      </div>
      {error && <Error error={error} />}
    </div>
  );
}

export default Register;
