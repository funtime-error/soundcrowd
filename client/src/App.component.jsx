import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Error from 'components/shared/Error.component';
import Header from 'components/shared/Header.component';
import Loading from 'components/shared/Loading.component';
import Profile from 'pages/Profile/Profile.component';
import Tracks from 'pages/Tracks/Tracks.component';

export const UserContext = React.createContext();

function App() {
  const { loading, error, data } = useQuery(ME_QUERY, {
    fetchPolicy: 'cache-and-network'
  });

  if (loading) return <Loading />;
  if (error) return <Error error={error} />;

  const currentUser = data.me;

  return (
    <Router>
      <UserContext.Provider value={currentUser}>
        <Header currentUser={currentUser} />
        <Switch>
          <Route path="/" component={Tracks} exact />
          <Route path="/profile/:id" component={Profile} />
        </Switch>
      </UserContext.Provider>
    </Router>
  );
}

export const ME_QUERY = gql`
  {
    me {
      id
      username
      email
      likeSet {
        track {
          id
        }
      }
    }
  }
`;

export default App;
