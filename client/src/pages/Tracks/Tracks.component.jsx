import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import CreateTrack from 'components/Tracks/CreateTrack.component';
import Error from 'components/shared/Error.component';
import Loading from 'components/shared/Loading.component';
import SearchTracks from 'components/Tracks/SearchTracks.component';
import TrackList from 'components/Tracks/TrackList.component';

function Tracks() {
  const [searchResults, setSearchResults] = useState([]);

  const { loading, error, data } = useQuery(GET_TRACKS_QUERY);

  if (loading) return <Loading />;
  if (error) return <Error error={error} />;

  const tracks = searchResults.length ? searchResults : data.tracks;

  return (
    <div className="container">
      <SearchTracks setSearchResults={setSearchResults} />
      <CreateTrack />
      <TrackList tracks={tracks} />
    </div>
  );
}

export const GET_TRACKS_QUERY = gql`
  query getTracksQuery {
    tracks {
      id
      title
      description
      url
      likes {
        id
      }
      postedBy {
        id
        username
      }
    }
  }
`;

export default Tracks;
