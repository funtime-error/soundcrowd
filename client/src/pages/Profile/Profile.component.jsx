import React from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

import AudioPlayer from 'components/shared/AudioPlayer.component';
import Error from 'components/shared/Error.component';
import LikeTrack from 'components/Tracks/LikeTrack.component';
import Loading from 'components/shared/Loading.component';
import TrackList from 'components/Tracks/TrackList.component';

function Profile({ match }) {
  const { id } = match.params;
  const { data, error, loading } = useQuery(PROFILE_QUERY, {
    variables: { id }
  });

  if (loading) return <Loading />;
  if (error) return <Error error={error} />;

  return (
    <div>
      <section className="hero is-dark">
        <div className="hero-body">
          <div className="level">
            <div className="level-left">
              <figure className="level-item image is-128x128">
                <img
                  className="is-rounded"
                  alt="profile avatar"
                  src="https://bulma.io/images/placeholders/256x256.png"
                />
              </figure>
              <h1 className="level-item title">{data.user.username}</h1>
            </div>
          </div>
        </div>
      </section>
      <section className="section columns">
        <div className="column">
          <h1 className="title">Uploads</h1>
          <TrackList tracks={data.user.trackSet} />
        </div>
        <div className="column">
          <h5 className="subtitle is-5">
            <span className="icon">
              <FontAwesomeIcon icon={faHeart} />
            </span>
            &nbsp;
            <span>
              {data.user.likeSet.length}{' '}
              {data.user.likeSet.length === 1 ? 'like' : 'likes'}
            </span>
          </h5>
          <hr />
          {data.user.likeSet.map(({ track }) => (
            <article className="media" key={track.id}>
              <figure className="media-left">
                <p className="image is-64x64">
                  <img
                    src="https://bulma.io/images/placeholders/256x256.png"
                    alt="album cover"
                  />
                </p>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <Link to={`/profile/${track.postedBy.id}`}>
                      {track.postedBy.username}
                    </Link>
                    <br />
                    <span className="subtitle is-5">{track.title}</span>
                    <LikeTrack
                      trackId={track.id}
                      likeCount={track.likes.length}
                    />
                  </p>
                </div>
                <AudioPlayer url={track.url} />
              </div>
            </article>
          ))}
        </div>
      </section>
    </div>
  );
}

const PROFILE_QUERY = gql`
  query($id: Int!) {
    user(id: $id) {
      id
      username
      dateJoined
      likeSet {
        id
        track {
          id
          title
          url
          likes {
            id
          }
          postedBy {
            id
            username
          }
        }
      }
      trackSet {
        id
        title
        description
        url
        likes {
          id
        }
        postedBy {
          id
          username
        }
      }
    }
  }
`;

export default Profile;
