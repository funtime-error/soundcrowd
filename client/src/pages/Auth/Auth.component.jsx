import React, { useState } from 'react';

import Login from 'components/Auth/Login.component';
import Register from 'components/Auth/Register.component';

function Auth() {
  const [newUser, setNewUser] = useState(true);

  return (
    <div className="columns is-centered">
      <div className="column is-one-third">
        {newUser ? (
          <Register setNewUser={setNewUser} />
        ) : (
          <Login setNewUser={setNewUser} />
        )}
      </div>
    </div>
  );
}

export default Auth;
