# SoundCrowd

Music sharing web app inspired by SoundCloud that enables users to upload and share audio

## Getting Started

### Back End

In a terminal (or command prompt) window or tab, navigate to the `app/` directory from within the project root and install the dependencies.

```
pip install -r requirements.txt
```

In the same tab, run the development server

```
python manage.py runserver
```

You may need to execute with `python3` if you have both Python 2 and Python 3 installed on your machine.

> NOTE: Python 2 is no longer supported

### Front End

In a separate terminal (or command prompt) window or tab, navigate to the `client/` directory from within the project root and install the dependencies.

```
npm install
```

In the same tab, run the development server

```
npm run start
```

The app should automatically open in a new browser tab. If it doesn't, navigate to `http://localhost:3000` in a new browser window

## Built With

- Django
- GraphQL
- Graphene
- React
- Bulma CSS
